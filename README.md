# Project: isarakorn.prapatsorn.com

by: Iceyisaak

This project features:

- Portfolio Website
- Profile Section
- Portfolio Section
- Contact Form
- Carousel Slider
- VDO snapshot background for mobile/Vdo background for larger viewports

- Pure HTML/CSS (SASS)
- FontAwesome
- GoogleFonts

## Installation
1. `npm install` to create node_modules
2. `npm run start` to run the project on live-server
3. ENJOY!

